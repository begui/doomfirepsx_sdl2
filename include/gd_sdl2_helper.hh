#pragma once
#include <memory>
#include <ostream>
#include <string>
#include <type_traits>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#if USE_SDL2_TTF
#ifdef __EMSCRIPTEN__
#include <SDL_ttf.h>
#else
#include <SDL2/SDL_ttf.h>
#endif
#endif
#if USE_SDL2_MIXER
#include <SDL2/SDL_mixer.h>
#endif
#if USE_SDL2_IMAGE
#include <SDL2/SDL_image.h>
#endif

inline std::ostream &operator<< ( std::ostream &os, const SDL_Rect &rec ) {  //
  return os << rec.x << " " << rec.y << " " << rec.w << " " << rec.h;
}
inline std::ostream &operator<< ( std::ostream &os, const SDL_Point &point ) {  //
  return os << point.x << " " << point.y;
}
inline std::ostream &operator<< ( std::ostream &os, const SDL_Color &color ) {  //
  return os << ( int ) color.r << " " << ( int ) color.g << " " << ( int ) color.b << " " << ( int ) color.a;
}

namespace sdl2 {

namespace internal {
constexpr auto DEFAULT_PIXELFORMAT = SDL_PIXELFORMAT_RGBA8888;
template < typename T >
constexpr auto to_type ( T t ) {
  return static_cast< typename std::underlying_type< T >::type > ( t );
}

template < typename TO, typename T >
constexpr TO to_type ( T t ) {
  return static_cast< TO > ( to_type ( t ) );
}
}  // namespace internal

enum class SUBSYSTEM : std::int8_t {
  //
  CORE = 0x00,
  //
  TTF = 0x01,
  //
  IMAGE = 0x02,
  //
  AUDIO = 0x04
};

enum class INIT : std::int32_t {
  //
  TIMER = SDL_INIT_TIMER,
  //
  AUDIO = SDL_INIT_AUDIO,
  //
  VIDEO = SDL_INIT_VIDEO,
  //
  JOYSTICK = SDL_INIT_JOYSTICK,
  //
  HAPTIC = SDL_INIT_HAPTIC,
  //
  CONTROLER = SDL_INIT_GAMECONTROLLER,
  //
  EVENTS = SDL_INIT_EVENTS,
  //
  EVERYTHING = SDL_INIT_EVERYTHING
};

enum class WINDOW : std::uint32_t {
  // full screen
  FULLSCREEN = SDL_WINDOW_FULLSCREEN,
  // full screen at current desktop rez
  FULLSCREEN_DESKTOP = SDL_WINDOW_FULLSCREEN_DESKTOP,
  // opengl window
  OPENGL = SDL_WINDOW_OPENGL,
  // window is visible
  SHOWN = SDL_WINDOW_SHOWN,
  // window is not visible
  HIDDEN = SDL_WINDOW_HIDDEN,
  // no decorations
  BORDERLESS = SDL_WINDOW_BORDERLESS,
  // allows to be resizable
  RESIZEABLE = SDL_WINDOW_RESIZABLE,
  // window is minimized
  MINIMIZED = SDL_WINDOW_MINIMIZED,
  // window is maximized
  MAXIMIZED = SDL_WINDOW_MAXIMIZED,
  // window has grab input focus
  INPUTGRAB = SDL_WINDOW_INPUT_GRABBED,
  // high dpi if supported
  HIGHDPI = SDL_WINDOW_ALLOW_HIGHDPI
};

enum class WINDOW_STATE {
  MINIMIZE,  //
  MAXIMIZE,
  RESTORE,
  HIDE,
  SHOW,
  RAISE
};

enum RENDERER : std::uint32_t {
  // software acceleration
  SOFTWARE = SDL_RENDERER_SOFTWARE,
  // hardware acceleration
  ACCELERATED = SDL_RENDERER_ACCELERATED,
  // allows vsync
  VSYNC = SDL_RENDERER_PRESENTVSYNC,
  // supports rendering to a texture
  TEXTURE = SDL_RENDERER_TARGETTEXTURE
};

enum class TEXTUREACCESS : std::int32_t {
  STATIC = SDL_TEXTUREACCESS_STATIC,
  STREAMING = SDL_TEXTUREACCESS_STREAMING,
  TARGET = SDL_TEXTUREACCESS_TARGET
};

enum class BLENDMODE : std::int32_t {
  // None:
  // dstRGBA = srcRGBA
  NONE = SDL_BLENDMODE_NONE,
  // Alpha:
  // dstRGB = (srcRGB * srcA) + (dstRGB * (1-srcA))
  // dstA = srcA + (dstA * (1-srcA))
  BLEND = SDL_BLENDMODE_BLEND,
  // Additive blending:
  // dstRGB = (srcRGB * srcA) + dstRGB
  // dstA = dstA
  ADD = SDL_BLENDMODE_ADD,
  // Color modulate:
  // dstRGB = srcRGB * dstRGB
  // dstA = dstA
  MOD = SDL_BLENDMODE_MOD
};

enum FLIP : std::int32_t {
  NONE = SDL_FLIP_NONE,  //
  HORIZONTAL = SDL_FLIP_HORIZONTAL,
  VERTICAL = SDL_FLIP_VERTICAL
};

#if USE_SDL2_MIXER
enum class AUDIO_OPERATION {
  PAUSE,  //
  RESUME,
  REWIND,
  STOP
};
enum class MUSIC_STATE : std::int8_t {
  PAUSED,  //
  PLAYING
};
#endif

#if USE_SDL2_IMAGE
enum class IMAGE : std::uint32_t {
  PNG = IMG_INIT_PNG,  //
  JPG = IMG_INIT_JPG,
  TIF = IMG_INIT_TIF
};
#endif

inline auto timer ( bool highprecision ) {
  if ( highprecision ) {
    static auto period = 1.0 / SDL_GetPerformanceFrequency ( );
    return SDL_GetPerformanceCounter ( ) * period;
  }
  return SDL_GetTicks ( ) / 1000.0;
}

class SDL2Exception : public std::runtime_error {
 public:
  explicit SDL2Exception ( const char *message )
      : std::runtime_error ( std::string ( message ) + " SDL_GetError() " + SDL_GetError ( ) ) {}
  explicit SDL2Exception ( const std::string &message )
      : std::runtime_error ( message + " SDL_GetError() " + SDL_GetError ( ) ) {}
  virtual ~SDL2Exception ( ) throw ( ) {}
};

struct SDLDeleter {
  void operator( ) ( SDL_Window *ptr ) {
    if ( ptr != nullptr ) {
      SDL_DestroyWindow ( ptr );
    }
  }
  void operator( ) ( SDL_Renderer *ptr ) {
    if ( ptr != nullptr ) {
      SDL_DestroyRenderer ( ptr );
    }
  }
  void operator( ) ( SDL_Surface *ptr ) {
    if ( ptr != nullptr ) {
      SDL_FreeSurface ( ptr );
    }
  }
  void operator( ) ( SDL_Texture *ptr ) {
    if ( ptr != nullptr ) {
      SDL_DestroyTexture ( ptr );
    }
  }
  void operator( ) ( SDL_GLContext *ptr ) {
    if ( ptr != nullptr ) {
      SDL_GL_DeleteContext ( &( *ptr ) );
    }
  }
  void operator( ) ( SDL_RWops *ptr ) {
    if ( ptr != nullptr ) {
      SDL_RWclose ( ptr );
    }
  }
  void operator( ) ( SDL_PixelFormat *ptr ) {
    if ( ptr != nullptr ) {
      SDL_FreeFormat ( ptr );
    }
  }
#if USE_SDL2_TTF
  void operator( ) ( TTF_Font *ptr ) {
    if ( ptr != nullptr ) {
      TTF_CloseFont ( ptr );
    }
  }
#endif
#if USE_SDL2_MIXER
  void operator( ) ( Mix_Chunk *ptr ) {
    if ( ptr != nullptr ) {
      Mix_FreeChunk ( ptr );
    }
  }
  void operator( ) ( Mix_Music *ptr ) {
    if ( ptr != nullptr ) {
      Mix_FreeMusic ( ptr );
    }
  }
#endif
};

using WindowUniqPtr = std::unique_ptr< SDL_Window, SDLDeleter >;
using WindowShrdPtr = std::shared_ptr< SDL_Window >;
using WindowWeakPtr = std::weak_ptr< SDL_Window >;

using RendererUniqPtr = std::unique_ptr< SDL_Renderer, SDLDeleter >;
using RendererShrdPtr = std::shared_ptr< SDL_Renderer >;
using RendererWeakPtr = std::weak_ptr< SDL_Renderer >;

using SurfaceUniqPtr = std::unique_ptr< SDL_Surface, SDLDeleter >;
using SurfaceShrdPtr = std::shared_ptr< SDL_Surface >;
using SurfaceWeakPtr = std::weak_ptr< SDL_Surface >;

using TextureUniqPtr = std::unique_ptr< SDL_Texture, SDLDeleter >;
using TextureShrdPtr = std::shared_ptr< SDL_Texture >;
using TextureWeakPtr = std::weak_ptr< SDL_Texture >;

using RWopsUniqPtr = std::unique_ptr< SDL_RWops, SDLDeleter >;
using PixelFormatUniqPtr = std::unique_ptr< SDL_PixelFormat, SDLDeleter >;

#if USE_SDL2_TTF
using TTFUniqPtr = std::unique_ptr< TTF_Font, SDLDeleter >;
using TTFShrdPtr = std::shared_ptr< TTF_Font >;
using TTFPWeakPtr = std::weak_ptr< TTF_Font >;

enum class TTFRENDERMODE : std::int8_t {
  BLENDED,  //
  SHADED,
  SOLID
};

enum TTFStyle : std::int32_t {
  NORMAL = TTF_STYLE_NORMAL,  //
  BOLD = TTF_STYLE_BOLD,
  ITALIC = TTF_STYLE_ITALIC,
  UNDERLINE = TTF_STYLE_UNDERLINE,
  STRIKETHROUGH = TTF_STYLE_STRIKETHROUGH
};

#endif
#if USE_SDL2_MIXER
using MixChunkUniqPtr = std::unique_ptr< Mix_Chunk, SDLDeleter >;
using MixChunkShrdPtr = std::shared_ptr< Mix_Chunk >;
using MixChunkWeakPtr = std::weak_ptr< Mix_Chunk >;

using MixMusicUniqPtr = std::unique_ptr< Mix_Music, SDLDeleter >;
using MixMusicShrdPtr = std::shared_ptr< Mix_Music >;
using MixMusicWeakPtr = std::weak_ptr< Mix_Music >;

enum class AUDIO {
  FLAC = MIX_INIT_FLAC,  //
  MOD = MIX_INIT_MOD,
  MP3 = MIX_INIT_MP3,
  OGG = MIX_INIT_OGG
};
#endif
inline bool isInit ( int flag ) noexcept { return ( SDL_WasInit ( flag ) & flag ) != 0u; }
inline bool isJoyStickEnabled ( ) noexcept { return isInit ( SDL_INIT_JOYSTICK ); }
inline bool isHapticEnabled ( ) noexcept { return SDL_WasInit ( SDL_INIT_HAPTIC ) != 0u; }
inline bool isGameControllerEnabled ( ) noexcept { return SDL_WasInit ( SDL_INIT_GAMECONTROLLER ) != 0u; }
inline bool isEventsEnabled ( ) noexcept { return SDL_WasInit ( SDL_INIT_EVENTS ) != 0u; }
inline bool isAudioEnabled ( ) noexcept { return SDL_WasInit ( SDL_INIT_AUDIO ) != 0u; }
inline bool isTimerEnabled ( ) noexcept { return SDL_WasInit ( SDL_INIT_TIMER ) != 0u; }
inline bool isVideoEnabled ( ) noexcept { return SDL_WasInit ( SDL_INIT_VIDEO ) != 0u; }
inline bool isEverythingEnabled ( ) noexcept { return SDL_WasInit ( SDL_INIT_EVERYTHING ) != 0u; }
#if USE_SDL2_TTF
inline bool isTTFEnabled ( ) { return TTF_WasInit ( ) != 0 ? true : false; }
#endif

namespace cpu {
inline bool has_3dnow ( ) noexcept { return SDL_Has3DNow ( ) == SDL_TRUE; }
inline bool has_altivec ( ) noexcept { return SDL_HasAltiVec ( ) == SDL_TRUE; }
inline bool has_avx ( ) noexcept { return SDL_HasAVX ( ) == SDL_TRUE; }
inline bool has_avx2 ( ) noexcept { return SDL_HasAVX2 ( ) == SDL_TRUE; }
inline bool has_mmx ( ) noexcept { return SDL_HasMMX ( ) == SDL_TRUE; }
inline bool has_rdtsc ( ) noexcept { return SDL_HasRDTSC ( ) == SDL_TRUE; }
inline bool has_sse ( ) noexcept { return SDL_HasSSE ( ) == SDL_TRUE; }
inline bool has_sse2 ( ) noexcept { return SDL_HasSSE2 ( ) == SDL_TRUE; }
inline bool has_sse3 ( ) noexcept { return SDL_HasSSE3 ( ) == SDL_TRUE; }
inline bool has_sse41 ( ) noexcept { return SDL_HasSSE41 ( ) == SDL_TRUE; }
inline bool has_sse42 ( ) noexcept { return SDL_HasSSE42 ( ) == SDL_TRUE; }

}  // namespace cpu

/**
 * Initializes sdl, don't forget to call SDL_Quit
 */
inline void quit_sdl ( ) { std::atexit (::SDL_Quit ); }
inline void init_sdl ( const std::int32_t flags = internal::to_type ( INIT::EVERYTHING ), const bool autoquit = true ) {
  SDL_SetMainReady ( );
  if ( SDL_Init ( flags ) != 0 ) {
    throw SDL2Exception ( "Failed to initialize sdl2." );
  }
  if ( autoquit ) {
    quit_sdl ( );
  }
}

#if USE_SDL2_TTF
inline void quit_ttf ( ) {
  if ( isTTFEnabled ( ) ) {
    std::atexit (::TTF_Quit );
  }
}
inline void init_ttf ( const bool autoquit = true ) {
  if ( TTF_Init ( ) != 0 ) {
    throw SDL2Exception ( "Failed to initialize sdl2 ttf." );
  }
  if ( autoquit ) {
    quit_ttf ( );
  }
}
#endif

#if USE_SDL2_MIXER
inline void quit_audio ( ) { std::atexit ( Mix_CloseAudio ); }
inline void quit_mix ( ) { std::atexit ( Mix_Quit ); }
inline void init_mixer ( const std::int32_t frequency, const std::int32_t stereochannels, const std::int32_t chunksize,
                         const std::int32_t flags = internal::to_type ( AUDIO::OGG ), const bool autoquit = true ) {
  if ( -1 == Mix_OpenAudio ( frequency, MIX_DEFAULT_FORMAT, stereochannels, chunksize ) ) {
    throw sdl2::SDL2Exception ( "Failed to initialize SDL2_mixer." );
  }
  if ( autoquit ) {
    quit_audio ( );
  }
  //
  // SDL Mix init
  //
  auto result = Mix_Init ( flags );
  if ( result != flags ) {
    throw sdl2::SDL2Exception ( "Failed to initialize SDL2_mixer." );  // Error: " + Mix_GetError ( ) );
  }
  if ( autoquit ) {
    quit_mix ( );
  }
}

#endif

#if USE_SDL2_IMAGE
inline void quit_image ( ) { std::atexit (::IMG_Quit ); }
inline void init_image ( const bool autoquit = false, const std::int32_t flags = internal::to_type ( IMAGE::PNG ) ) {
  auto imgFlagsInit = IMG_Init ( flags );
  if ( ( imgFlagsInit & flags ) != flags ) {
    throw sdl2::SDL2Exception ( "Failed to initialize SDL2_image." );  //, IMG_GetError);
  }
  if ( autoquit ) {
    quit_image ( );
  }
}
#endif

inline RWopsUniqPtr rw_FromFile ( const char *file, const char *mode ) {
  auto rw = SDL_RWFromFile ( file, mode );
  if ( rw == nullptr ) {
    throw SDL2Exception ( "Failed on RWops." );
  }
  return RWopsUniqPtr ( rw );
}

inline RWopsUniqPtr rw_FromConstMem ( const std::uint8_t *buffer, const int size ) {
  auto rw = SDL_RWFromConstMem ( buffer, size );
  if ( rw == nullptr ) {
    throw SDL2Exception ( "Failed on RWops." );
  }
  return RWopsUniqPtr ( rw );
}

inline RWopsUniqPtr rw_FromMem ( std::uint8_t *buffer, const int size ) {  //
  auto rw = SDL_RWFromMem ( buffer, size );
  if ( rw == nullptr ) {
    throw SDL2Exception ( "Failed on RWops." );
  }
  return RWopsUniqPtr ( rw );
}

inline const std::string base_path ( ) noexcept {
  using sdl_path = std::unique_ptr< char, decltype ( &SDL_free ) >;
  auto cstr = sdl_path ( SDL_GetBasePath ( ), SDL_free );
  if ( cstr ) {
    const std::string sstr{cstr.get ( )};
    return sstr;
  }
  return {};
}
inline const std::string pref_path ( const char *org, const char *app ) noexcept {
  using sdl_path = std::unique_ptr< char, decltype ( &SDL_free ) >;
  auto cstr = sdl_path ( SDL_GetPrefPath ( org, app ), SDL_free );
  if ( cstr ) {
    const std::string sstr{cstr.get ( )};
    return sstr;
  }
  return {};
}
/**
 *
 */
inline auto get_version ( SUBSYSTEM subSystem ) noexcept {
  SDL_version version{0, 0, 0};
  switch ( subSystem ) {
    case SUBSYSTEM::CORE:
      SDL_VERSION ( &version );
      break;
    case SUBSYSTEM::IMAGE:
#if USE_SDL2_IMAGE
      SDL_IMAGE_VERSION ( &version );
#endif
      break;
    case SUBSYSTEM::TTF:
#if USE_SDL2_TTF
      SDL_TTF_VERSION ( &version );
#endif
      break;
    case SUBSYSTEM::AUDIO:
#if USE_SDL2_MIXER
      SDL_MIXER_VERSION ( &version );
#endif
      break;
  }
  return version;
}
/**
 *
 **/
inline auto get_linked_version ( SUBSYSTEM subSystem ) noexcept {
  SDL_version linked{0, 0, 0};
  switch ( subSystem ) {
    case SUBSYSTEM::CORE:
      SDL_GetVersion ( &linked );
      break;
    case SUBSYSTEM::IMAGE:
#if USE_SDL2_IMAGE
      linked = *IMG_Linked_Version ( );
#endif
      break;
    case SUBSYSTEM::TTF:
#if USE_SDL2_TTF
      linked = *TTF_Linked_Version ( );
#endif
      break;
    case SUBSYSTEM::AUDIO:
#if USE_SDL2_MIXER
      linked = *Mix_Linked_Version ( );
#endif
      break;
    default:
      break;
  }
  return linked;
}

// Blend Mode
inline BLENDMODE get_blend_mode ( SDL_Renderer *ptr ) {
  SDL_BlendMode blendmode = SDL_BLENDMODE_NONE;
  if ( SDL_GetRenderDrawBlendMode ( ptr, &blendmode ) != 0 ) {
    throw SDL2Exception ( "Failed to get Renderer Blend mode state." );
  }
  return static_cast< BLENDMODE > ( blendmode );
}
inline BLENDMODE get_blend_mode ( SDL_Texture *ptr ) {
  SDL_BlendMode blendmode = SDL_BLENDMODE_NONE;
  if ( SDL_GetTextureBlendMode ( ptr, &blendmode ) != 0 ) {
    throw SDL2Exception ( "Failed to get Texture Blend mode state." );
  }
  return static_cast< BLENDMODE > ( blendmode );
}
inline BLENDMODE get_blend_mode ( SDL_Surface *ptr ) {
  SDL_BlendMode blendmode = SDL_BLENDMODE_NONE;
  if ( SDL_GetSurfaceBlendMode ( ptr, &blendmode ) != 0 ) {
    throw SDL2Exception ( "Failed to get Surface Blend mode state." );
  }
  return static_cast< BLENDMODE > ( blendmode );
}
inline void set_blend_mode ( SDL_Renderer *ptr, BLENDMODE mode = BLENDMODE::NONE ) {
  if ( SDL_SetRenderDrawBlendMode ( ptr, internal::to_type< SDL_BlendMode > ( mode ) ) != 0 ) {
    throw SDL2Exception ( "Failed to set Renderer Blend mode state." );
  }
}
inline void set_blend_mode ( SDL_Texture *ptr, BLENDMODE mode = BLENDMODE::NONE ) {
  if ( SDL_SetTextureBlendMode ( ptr, internal::to_type< SDL_BlendMode > ( mode ) ) != 0 ) {
    throw SDL2Exception ( "Failed to set Texture Blend mode state." );
  }
}
inline void set_blend_mode ( SDL_Surface *ptr, BLENDMODE mode = BLENDMODE::NONE ) {
  if ( SDL_SetSurfaceBlendMode ( ptr, internal::to_type< SDL_BlendMode > ( mode ) ) != 0 ) {
    throw SDL2Exception ( "Failed to set Surface Blend mode state." );
  }
}
// Alpha Mod
inline std::uint8_t get_alpha_mod ( SDL_Texture *ptr ) {
  std::uint8_t alpha;
  if ( SDL_GetTextureAlphaMod ( ptr, &alpha ) != 0 ) {
    throw SDL2Exception ( "Failed to get Texture Alpha Mod state." );
  }
  return alpha;
}
inline std::uint8_t get_alpha_mod ( SDL_Surface *ptr ) {
  std::uint8_t alpha;
  if ( SDL_GetSurfaceAlphaMod ( ptr, &alpha ) != 0 ) {
    throw SDL2Exception ( "Failed to get Surface Alpha Mod state." );
  }
  return alpha;
}
inline void set_alpha_mod ( SDL_Texture *ptr, std::uint8_t alpha = SDL_ALPHA_OPAQUE ) {
  SDL_SetTextureAlphaMod ( ptr, alpha );
}
inline void set_alpha_mod ( SDL_Surface *ptr, std::uint8_t alpha = SDL_ALPHA_OPAQUE ) {
  SDL_SetSurfaceAlphaMod ( ptr, alpha );
}
// Color Mod
inline SDL_Color get_color_mod ( SDL_Texture *ptr ) {
  std::uint8_t r, g, b;
  SDL_GetTextureColorMod ( ptr, &r, &g, &b );
  return {r, g, b, 0};
}
inline SDL_Color get_color_mod ( SDL_Surface *ptr ) {
  std::uint8_t r, g, b;
  SDL_GetSurfaceColorMod ( ptr, &r, &g, &b );
  return {r, g, b, 0};
}
inline void set_color_mod ( SDL_Texture *ptr, const SDL_Color &color ) {
  SDL_SetTextureColorMod ( ptr, color.r, color.g, color.b );
}
inline void set_color_mod ( SDL_Surface *ptr, const SDL_Color &color ) {
  SDL_SetSurfaceColorMod ( ptr, color.r, color.g, color.b );
}
/**
 * Window
 **/
template < typename T >
T create_window ( const SDL_Point &size, const SDL_Point &position = {SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED},
                  std::uint32_t flags = internal::to_type ( WINDOW::SHOWN ) );
template <>
inline SDL_Window *create_window ( const SDL_Point &size, const SDL_Point &position, std::uint32_t flags ) {
  return SDL_CreateWindow ( "", position.x, position.y, size.x, size.y, flags );
}
template <>
inline WindowUniqPtr create_window ( const SDL_Point &size, const SDL_Point &position, std::uint32_t flags ) {
  return WindowUniqPtr ( create_window< SDL_Window * > ( size, position, flags ) );
}
template <>
inline WindowShrdPtr create_window ( const SDL_Point &size, const SDL_Point &position, std::uint32_t flags ) {
  return WindowShrdPtr ( create_window< WindowUniqPtr > ( size, position, flags ) );
}
/**
 * Renderer
 **/
template < typename T >
T create_renderer ( SDL_Window *window, BLENDMODE blendmode = BLENDMODE::NONE,
                    std::uint32_t flags = RENDERER::VSYNC | RENDERER::ACCELERATED );

template <>
inline SDL_Renderer *create_renderer ( SDL_Window *window, BLENDMODE blendmode, std::uint32_t flags ) {
  if ( window == nullptr ) {
    throw SDL2Exception ( "Unable to create renderer, Window is null" );
  }
  auto renderer = SDL_CreateRenderer ( window, -1, flags );
  set_blend_mode ( renderer, blendmode );
  return renderer;
}
template <>
inline RendererUniqPtr create_renderer ( SDL_Window *window, BLENDMODE blendmode, std::uint32_t flags ) {
  return RendererUniqPtr ( create_renderer< SDL_Renderer * > ( window, blendmode, flags ) );
}
template <>
inline RendererShrdPtr create_renderer ( SDL_Window *window, BLENDMODE blendmode, std::uint32_t flags ) {
  return RendererShrdPtr ( create_renderer< RendererUniqPtr > ( window, blendmode, flags ) );
}
/**
 *
 **/
inline auto get_window_size ( SDL_Window *window ) {
  int width, height;
  SDL_GetWindowSize ( window, &width, &height );
  return std::make_tuple ( width, height );
}
inline auto get_window_id ( SDL_Window *window ) { return SDL_GetWindowID ( window ); }
inline auto get_window_position ( SDL_Window *window ) {
  int width, height;
  SDL_GetWindowPosition ( window, &width, &height );
  return std::make_tuple ( width, height );
}
inline auto get_window_pixelformat ( SDL_Window *window ) { return SDL_GetWindowPixelFormat ( window ); }

inline auto create_pixelformat( SDL_Window *window ) {
  return PixelFormatUniqPtr(SDL_AllocFormat ( SDL_GetWindowPixelFormat ( window ) ));
}

inline void set_window_position ( SDL_Window *window, int x, int y ) { SDL_SetWindowPosition ( window, x, y ); }
inline auto get_window_aspect_ratio ( SDL_Window *window ) {
  auto size = get_window_size ( window );
  return std::get< 0 > ( size ) / std::get< 1 > ( size );
}
inline void set_window_title ( SDL_Window *window, const char *text ) { SDL_SetWindowTitle ( window, text ); }
inline void set_window_fullscreen ( SDL_Window *window, const bool fullscreen = true,
                                    const WINDOW windowflag = WINDOW::FULLSCREEN ) {
  if ( 0 != SDL_SetWindowFullscreen ( window, !fullscreen ? 0 : internal::to_type ( windowflag ) ) ) {
    throw SDL2Exception ( "Failed to full screen" );
  }
}
inline void set_window_size ( SDL_Window *window, std::int32_t width, std::int32_t height ) {
  SDL_SetWindowSize ( window, width, height );
}

inline auto get_window_refresh_rate ( SDL_Window *Window, const int defaultRefershRate = 60 ) {
  SDL_DisplayMode mode;
  if ( SDL_GetDesktopDisplayMode ( SDL_GetWindowDisplayIndex ( Window ), &mode ) != 0 || mode.refresh_rate == 0 ) {
    return defaultRefershRate;
  }
  return mode.refresh_rate;
}

inline auto set_window_state ( SDL_Window *window, WINDOW_STATE state ) {
  switch ( state ) {
    case WINDOW_STATE::SHOW:
      SDL_ShowWindow ( window );
      break;
    case WINDOW_STATE::HIDE:
      SDL_HideWindow ( window );
      break;
    case WINDOW_STATE::MAXIMIZE:
      SDL_MaximizeWindow ( window );
      break;
    case WINDOW_STATE::MINIMIZE:
      SDL_MinimizeWindow ( window );
      break;
    case WINDOW_STATE::RESTORE:
      SDL_RestoreWindow ( window );
      break;
    case WINDOW_STATE::RAISE:
      SDL_RaiseWindow ( window );
      break;
  }
}
/**
 *
 **/
inline void swap ( SDL_Window *window ) { SDL_GL_SwapWindow ( window ); }
inline void swap ( SDL_Renderer *renderer ) { SDL_RenderPresent ( renderer ); }

inline auto get_renderer_info ( SDL_Renderer *renderer ) {
  SDL_RendererInfo info;
  SDL_GetRendererInfo ( renderer, &info );
  return std::make_tuple ( info.name, info.max_texture_width, info.max_texture_height );
}
/**
 * Input Operations
 **/
inline bool keyboard ( SDL_Scancode scancode ) {
  static int numkeys = 0;
  static const uint8_t *state = SDL_GetKeyboardState ( &numkeys );
  return state[ scancode ] != 0;
}
/**
 *
 **/
inline void enable_mouse_grab ( SDL_Window *window, bool enable ) {
  if ( enable && SDL_GetRelativeMouseMode ( ) == SDL_TRUE ) {
    SDL_SetRelativeMouseMode ( SDL_FALSE );
    SDL_SetWindowGrab ( window, SDL_FALSE );
  } else {
    SDL_SetRelativeMouseMode ( SDL_TRUE );
    SDL_SetWindowGrab ( window, SDL_TRUE );
  }
}

inline void enable_mouse_cursor ( bool enable ) { SDL_ShowCursor ( enable ? SDL_ENABLE : SDL_DISABLE ); }

inline void set_transparent_pixel ( SDL_Surface *ptr, bool enable, int r = 0, int g = 0, int b = 0 ) {
  if ( ptr != nullptr ) {
    if ( SDL_SetColorKey ( ptr, enable ? SDL_TRUE : SDL_FALSE, SDL_MapRGB ( ptr->format, r, g, b ) ) != 0 ) {
      throw SDL2Exception ( "Failed to set color key" );
    }
  }
}
/**
 *
 */
template < typename T >
T create_surface ( int width, int height, int depth = 32 );

template <>
inline SDL_Surface *create_surface ( int width, int height, int depth ) {
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  auto rmask = 0xff000000;
  auto gmask = 0x00ff0000;
  auto bmask = 0x0000ff00;
  auto amask = 0x000000ff;
#else
  auto rmask = 0x000000ff;
  auto gmask = 0x0000ff00;
  auto bmask = 0x00ff0000;
  auto amask = 0xff000000;
#endif

  auto ptr = SDL_CreateRGBSurface ( 0, width, height, depth, rmask, gmask, bmask, amask );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create surface ." );
  }
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( int width, int height, int depth ) {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( width, height, depth ) );
}

template <>
inline SurfaceShrdPtr create_surface ( int width, int height, int depth ) {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( width, height, depth ) );
}
/**
 *
 */
template < typename T >
T create_surface ( int width, int height, int depth, const SDL_Color &color );

template <>
inline SDL_Surface *create_surface ( int width, int height, int depth, const SDL_Color &color ) {
  auto ptr = create_surface< SDL_Surface * > ( width, height, depth );
  set_transparent_pixel ( ptr, ptr->format != nullptr, color.r, color.g, color.b );
  return ptr;
}

template <>
inline SurfaceUniqPtr create_surface ( int width, int height, int depth, const SDL_Color &color ) {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( width, height, depth, color ) );
}

template <>
inline SurfaceShrdPtr create_surface ( int width, int height, int depth, const SDL_Color &color ) {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( width, height, depth, color ) );
}
/**
 *
 */
template < typename T >
T create_surface ( const char *filename );

template <>
inline SDL_Surface *create_surface ( const char *filename ) {
#if USE_SDL2_IMAGE
  auto ptr = IMG_Load ( filename );
#else
  auto ptr = SDL_LoadBMP ( filename );
#endif
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create surface ." );
  }
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( const char *filename ) {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( filename ) );
}
template <>
inline SurfaceShrdPtr create_surface ( const char *filename ) {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( filename ) );
}
template < typename T >
T create_surface ( const char *filename, const SDL_Color &color );

template <>
inline SDL_Surface *create_surface ( const char *filename, const SDL_Color &color ) {
  auto ptr = create_surface< SDL_Surface * > ( filename );
  set_transparent_pixel ( ptr, ptr->format != nullptr, color.r, color.g, color.b );
  return ptr;
}
template <>
inline SurfaceUniqPtr create_surface ( const char *filename, const SDL_Color &color ) {
  return SurfaceUniqPtr ( create_surface< SDL_Surface * > ( filename, color ) );
}
template <>
inline SurfaceShrdPtr create_surface ( const char *filename, const SDL_Color &color ) {
  return SurfaceShrdPtr ( create_surface< SurfaceUniqPtr > ( filename, color ) );
}
/**
 *
 */
inline void render_scale_quality ( const bool useNearest = true ) {
  if ( SDL_SetHint ( SDL_HINT_RENDER_SCALE_QUALITY, useNearest ? "nearest" : "linear" ) < 0 ) {
    throw SDL2Exception ( "Failed to set Render Scale Quality" );
  }
}
/**
 *
 */
inline auto get_texture_dimension ( SDL_Texture *ptr ) {
  int w, h = 0;
  if ( SDL_QueryTexture ( ptr, nullptr, nullptr, &w, &h ) != 0 ) {
    throw SDL2Exception ( "Failed to query Texture." );
  }
  return std::make_tuple ( w, h );
}
/**
 *
 */
template < typename T >
T surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface );

template <>
inline SDL_Texture *surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) {
  return SDL_CreateTextureFromSurface ( renderer, surface.get ( ) );
}
template <>
inline TextureUniqPtr surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) {
  return TextureUniqPtr (
      surface_to_texture< SDL_Texture * > ( renderer, std::forward< SurfaceUniqPtr > ( surface ) ) );
}
template <>
inline TextureShrdPtr surface_to_texture ( SDL_Renderer *renderer, SurfaceUniqPtr &&surface ) {
  return TextureShrdPtr (
      surface_to_texture< TextureUniqPtr > ( renderer, std::forward< SurfaceUniqPtr > ( surface ) ) );
}
/**
 *
 */
template < typename T >
T create_texture ( SDL_Renderer *renderer, int pixelFormat, TEXTUREACCESS textureAccess, int width, int height );

template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, int pixelFormat, TEXTUREACCESS textureAccess, int width,
                                   int height ) {
  auto ptr = SDL_CreateTexture ( renderer, pixelFormat, internal::to_type ( textureAccess ), width, height );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create texture." );
  }
  return ptr;
}
template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, int pixelFormat, TEXTUREACCESS textureAccess, int width,
                                     int height ) {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( renderer, pixelFormat, textureAccess, width, height ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, int pixelFormat, TEXTUREACCESS textureAccess, int width,
                                     int height ) {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( renderer, pixelFormat, textureAccess, width, height ) );
}
//
template < typename T >
T create_texture ( SDL_Window *window, SDL_Renderer *renderer, TEXTUREACCESS textureAccess, int width, int height );

template <>
inline SDL_Texture *create_texture ( SDL_Window *window, SDL_Renderer *renderer, TEXTUREACCESS textureAccess, int width,
                                   int height ) {
  return create_texture< SDL_Texture * > ( renderer, get_window_pixelformat ( window ), textureAccess, width, height );
}
template <>
inline TextureUniqPtr create_texture ( SDL_Window *window, SDL_Renderer *renderer, TEXTUREACCESS textureAccess, int width,
                                     int height ) {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( window, renderer, textureAccess, width, height ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Window *window, SDL_Renderer *renderer, TEXTUREACCESS textureAccess, int width,
                                     int height ) {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( window, renderer, textureAccess, width, height ) );
}
//
template < typename T >
T create_texture_target ( SDL_Window *window, SDL_Renderer *renderer, int width, int height );

template <>
inline SDL_Texture *create_texture_target ( SDL_Window *window, SDL_Renderer *renderer, int width, int height ) {
  return create_texture< SDL_Texture * > ( window, renderer, TEXTUREACCESS::TARGET, width, height );
}
template <>
inline TextureUniqPtr create_texture_target ( SDL_Window *window, SDL_Renderer *renderer, int width, int height ) {
  return TextureUniqPtr ( create_texture_target< SDL_Texture * > ( window, renderer, width, height ) );
}
template <>
inline TextureShrdPtr create_texture_target ( SDL_Window *window, SDL_Renderer *renderer, int width, int height ) {
  return TextureShrdPtr ( create_texture_target< TextureUniqPtr > ( window, renderer, width, height ) );
}

/**
 *
 **/
template < typename T >
T create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color );

template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( !surfacePtr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return surface_to_texture< SDL_Texture * > ( renderer, std::move ( surfacePtr ) );
}

template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( !surfacePtr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return surface_to_texture< TextureUniqPtr > ( renderer, std::move ( surfacePtr ) );
}

template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const char *filename, const SDL_Color &color ) {
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename, color );
  if ( !surfacePtr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return surface_to_texture< TextureShrdPtr > ( renderer, std::move ( surfacePtr ) );
}

template < typename T >
T create_texture ( SDL_Renderer *renderer, const char *filename );
template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const char *filename ) {
#if USE_SDL2_IMAGE
  auto ptr = IMG_LoadTexture ( renderer, filename );
#else
  auto surfacePtr = create_surface< SurfaceUniqPtr > ( filename );
  auto ptr = surface_to_texture< SDL_Texture * > ( renderer, std::move ( surfacePtr ) );
#endif
  if ( !ptr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return ptr;
}
template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const char *filename ) {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( renderer, filename ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const char *filename ) {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( renderer, filename ) );
}
/*
 *
 */

template < typename T >
T create_texture ( SDL_Renderer *renderer, const std::uint8_t *buffer, std::size_t size );
template <>
inline SDL_Texture *create_texture ( SDL_Renderer *renderer, const std::uint8_t *buffer, std::size_t size ) {
  auto rw = SDL_RWFromConstMem ( buffer, size );
  if ( rw == nullptr ) {
    throw SDL2Exception ( "Failed on RWops." );
  }
#if USE_SDL2_IMAGE
  auto ptr = IMG_LoadTexture_RW ( renderer, rw, 1 );
#else
  auto surfacePtr = SurfaceUniqPtr ( SDL_LoadBMP_RW ( rw, 1 ) );
  auto ptr = surface_to_texture< SDL_Texture * > ( renderer, std::move ( surfacePtr ) );
#endif
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create Texture." );
  }
  return ptr;
}
template <>
inline TextureUniqPtr create_texture ( SDL_Renderer *renderer, const std::uint8_t *buffer, std::size_t size ) {
  return TextureUniqPtr ( create_texture< SDL_Texture * > ( renderer, buffer, size ) );
}
template <>
inline TextureShrdPtr create_texture ( SDL_Renderer *renderer, const std::uint8_t *buffer, std::size_t size ) {
  return TextureShrdPtr ( create_texture< TextureUniqPtr > ( renderer, buffer, size ) );
}

#if USE_SDL2_TTF

template < typename T >
T ttf_load ( const std::uint8_t *buffer, std::size_t size, int pointSize, int index = 0 );
template <>
inline TTF_Font *ttf_load ( const std::uint8_t *buffer, std::size_t size, int pointSize, int index ) {
  auto rw = SDL_RWFromConstMem ( buffer, size );
  if ( rw == nullptr ) {
    throw SDL2Exception ( "Failed on RWops." );
  }
  auto ptr = TTF_OpenFontIndexRW ( rw, 1, pointSize, index );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create ttf." );
  }
  return ptr;
}
template <>
inline TTFUniqPtr ttf_load ( const std::uint8_t *buffer, std::size_t size, int pointSize, int index ) {
  return TTFUniqPtr ( ttf_load< TTF_Font * > ( buffer, size, pointSize, index ) );
}
template <>
inline TTFShrdPtr ttf_load ( const std::uint8_t *buffer, std::size_t size, int pointSize, int index ) {
  return TTFShrdPtr ( ttf_load< TTFUniqPtr > ( buffer, size, pointSize, index ) );
}

template < typename T >
T ttf_load ( SDL_RWops *rw, int pointSize, int index = 0 );
template <>
inline TTF_Font *ttf_load ( SDL_RWops *rw, int pointSize, int index ) {
  auto ptr = TTF_OpenFontIndexRW ( rw, 0, pointSize, index );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create ttf." );
  }
  return ptr;
}
template <>
inline TTFUniqPtr ttf_load ( SDL_RWops *rw, int pointSize, int index ) {
  return TTFUniqPtr ( ttf_load< TTF_Font * > ( rw, pointSize, index ) );
}
template <>
inline TTFShrdPtr ttf_load ( SDL_RWops *rw, int pointSize, int index ) {
  return TTFShrdPtr ( ttf_load< TTFUniqPtr > ( rw, pointSize, index ) );
}

inline void set_ttf_style ( TTF_Font *font, TTFStyle style ) { TTF_SetFontStyle ( font, style ); }

inline void set_ttf_kerning ( TTF_Font *font, bool enable = true ) { TTF_SetFontKerning ( font, enable ); }

inline std::int32_t get_ttf_height ( const TTF_Font *font ) { return TTF_FontHeight ( font ); }
/**
 *
 **/
template < typename T >
T render_ttf ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg = {0, 0, 0, 0},
               const TTFRENDERMODE renderMode = TTFRENDERMODE::SOLID, bool asUTF8 = false );

template <>
inline SDL_Surface *render_ttf ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg,
                                 const TTFRENDERMODE renderMode, bool asUTF8 ) {
  SDL_Surface *surfacePtr{nullptr};
  switch ( renderMode ) {
    case TTFRENDERMODE::BLENDED:
      surfacePtr = ( asUTF8 ? TTF_RenderUTF8_Blended : TTF_RenderText_Blended ) ( font, text, fg );
      break;
    case TTFRENDERMODE::SHADED:
      surfacePtr = ( asUTF8 ? TTF_RenderUTF8_Shaded : TTF_RenderText_Shaded ) ( font, text, fg, bg );
      break;
    case TTFRENDERMODE::SOLID:
      surfacePtr = ( asUTF8 ? TTF_RenderUTF8_Solid : TTF_RenderText_Solid ) ( font, text, fg );
      break;
    default:
      break;
  }
  return surfacePtr;
}
template <>
inline SurfaceUniqPtr render_ttf ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg,
                                   const TTFRENDERMODE renderMode, bool asUTF8 ) {
  return SurfaceUniqPtr ( render_ttf< SDL_Surface * > ( font, text, fg, bg, renderMode ) );
}

template <>
inline SurfaceShrdPtr render_ttf ( TTF_Font *font, const char *text, const SDL_Color &fg, const SDL_Color &bg,
                                   TTFRENDERMODE renderMode, bool asUTF8 ) {
  return SurfaceShrdPtr ( render_ttf< SurfaceUniqPtr > ( font, text, fg, bg, renderMode ) );
}

/*
 *
 */
template < typename T >
T render_ttf ( TTF_Font *font, const std::uint16_t *text, const SDL_Color &fg, const SDL_Color &bg = {0, 0, 0, 0},
               TTFRENDERMODE renderMode = TTFRENDERMODE::SOLID );

template <>
inline SDL_Surface *render_ttf ( TTF_Font *font, const std::uint16_t *text, const SDL_Color &fg, const SDL_Color &bg,
                                 TTFRENDERMODE renderMode ) {
  SDL_Surface *surfacePtr = nullptr;
  switch ( renderMode ) {
    case TTFRENDERMODE::BLENDED:
      surfacePtr = TTF_RenderUNICODE_Blended ( font, text, fg );
      break;
    case TTFRENDERMODE::SHADED:
      surfacePtr = TTF_RenderUNICODE_Shaded ( font, text, fg, bg );
      break;
    case TTFRENDERMODE::SOLID:
      surfacePtr = TTF_RenderUNICODE_Solid ( font, text, fg );
      break;
    default:
      break;
  }
  return surfacePtr;
}
template <>
inline SurfaceUniqPtr render_ttf ( TTF_Font *font, const std::uint16_t *text, const SDL_Color &fg, const SDL_Color &bg,
                                   TTFRENDERMODE renderMode ) {
  return SurfaceUniqPtr ( render_ttf< SDL_Surface * > ( font, text, fg, bg, renderMode ) );
}

template <>
inline SurfaceShrdPtr render_ttf ( TTF_Font *font, const std::uint16_t *text, const SDL_Color &fg, const SDL_Color &bg,
                                   TTFRENDERMODE renderMode ) {
  return SurfaceShrdPtr ( render_ttf< SurfaceUniqPtr > ( font, text, fg, bg, renderMode ) );
}

#endif

#if USE_SDL2_MIXER

constexpr auto AUDIO_VOLUME_MAX{MIX_MAX_VOLUME};

template < typename T >
inline T audio_load ( const char *filename );

template <>
inline Mix_Chunk *audio_load ( const char *filename ) {
  auto ptr = Mix_LoadWAV ( filename );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create Mix Chunk." );
  }
}
template <>
inline MixChunkUniqPtr audio_load ( const char *filename ) {
  return MixChunkUniqPtr ( audio_load< Mix_Chunk * > ( filename ) );
}
template <>
inline MixChunkShrdPtr audio_load ( const char *filename ) {
  return MixChunkShrdPtr ( audio_load< MixChunkUniqPtr > ( filename ) );
}

template <>
inline Mix_Music *audio_load ( const char *filename ) {
  auto ptr = Mix_LoadMUS ( filename );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create Mix Music." );
  }
  return ptr;
}
template <>
inline MixMusicUniqPtr audio_load ( const char *filename ) {
  return MixMusicUniqPtr ( audio_load< Mix_Music * > ( filename ) );
}
template <>
inline MixMusicShrdPtr audio_load ( const char *filename ) {
  return MixMusicShrdPtr ( audio_load< MixMusicUniqPtr > ( filename ) );
}

template < typename T >
inline T audio_load ( const std::uint8_t *buffer, std::size_t size );
template <>
inline Mix_Chunk *audio_load ( const std::uint8_t *buffer, std::size_t size ) {
  auto rw = SDL_RWFromConstMem ( buffer, size );
  if ( rw == nullptr ) {
    throw SDL2Exception ( "Failed on RWops." );
  }
  auto ptr = Mix_LoadWAV_RW ( rw, 1 );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create Mix Chunk." );
  }
  return ptr;
}
template <>
inline MixChunkUniqPtr audio_load ( const std::uint8_t *buffer, std::size_t size ) {
  return MixChunkUniqPtr ( audio_load< Mix_Chunk * > ( buffer, size ) );
}
template <>
inline MixChunkShrdPtr audio_load ( const std::uint8_t *buffer, std::size_t size ) {
  return MixChunkShrdPtr ( audio_load< MixChunkUniqPtr > ( buffer, size ) );
}
template <>
inline Mix_Music *audio_load ( const std::uint8_t *buffer, std::size_t size ) {
  auto rw = SDL_RWFromConstMem ( buffer, size );
  if ( rw == nullptr ) {
    throw SDL2Exception ( "Failed on RWops." );
  }
  auto ptr = Mix_LoadMUS_RW ( rw, 1 );
  if ( ptr == nullptr ) {
    throw SDL2Exception ( "Failed to create Mix Music." );
  }
  return ptr;
}
template <>
inline MixMusicUniqPtr audio_load ( const std::uint8_t *buffer, std::size_t size ) {
  return MixMusicUniqPtr ( audio_load< Mix_Music * > ( buffer, size ) );
}
template <>
inline MixMusicShrdPtr audio_load ( const std::uint8_t *buffer, std::size_t size ) {
  return MixMusicShrdPtr ( audio_load< MixMusicUniqPtr > ( buffer, size ) );
}

//
// Sound functions
//
inline int sound_play ( Mix_Chunk *chunk, int channel = -1, int loop = 0 ) {
  int rtn = Mix_PlayChannel ( channel, chunk, loop );
  if ( -1 == rtn ) {
    throw SDL2Exception ( "Error playing sound" );
  }
  return rtn;
}
inline void sound_allocate_channels ( std::int32_t num ) { Mix_AllocateChannels ( num ); }
inline std::int32_t sound_get_channels ( ) { return Mix_AllocateChannels ( -1 ); }
inline int sound_volume ( int channel, int volume ) { return Mix_Volume ( channel, volume ); }
inline int sound_volume_all ( int volume ) { return sound_volume ( -1, volume ); }
inline void sound_operation ( AUDIO_OPERATION operation, int channel = -1 ) {
  // channel = -1 will affect all channels
  switch ( operation ) {
    case AUDIO_OPERATION::PAUSE:
      Mix_Pause ( channel );
      break;
    case AUDIO_OPERATION::RESUME:
      Mix_Resume ( channel );
      break;
    case AUDIO_OPERATION::STOP:
      Mix_HaltChannel ( channel );
    default:
      break;
  }
}

inline bool is_channel_playing ( int channel ) { return Mix_Playing ( channel ) != 0; }
inline int get_channels_playing ( ) { return Mix_Playing ( -1 ); }

inline void sound_callback ( void ( *callback ) ( int ) ) { Mix_ChannelFinished ( callback ); }
//
// Music functions
//
inline bool music_state ( MUSIC_STATE state ) {
  bool rtn{false};
  switch ( state ) {
    case MUSIC_STATE::PLAYING:
      rtn = Mix_PlayingMusic ( );
      break;
    case MUSIC_STATE::PAUSED:
      rtn = Mix_PausedMusic ( );
      break;
    default:
      break;
  }
  return rtn;
}

inline bool music_fading_none ( ) { return MIX_NO_FADING == Mix_FadingMusic ( ); }
inline bool music_fading_in ( ) { return MIX_FADING_IN == Mix_FadingMusic ( ); }
inline bool music_fading_out ( ) { return MIX_FADING_OUT == Mix_FadingMusic ( ); }

inline void music_operation ( AUDIO_OPERATION operation ) {
  switch ( operation ) {
    case AUDIO_OPERATION::REWIND:
      Mix_RewindMusic ( );
      break;
    case AUDIO_OPERATION::PAUSE:
      Mix_PauseMusic ( );
      break;
    case AUDIO_OPERATION::RESUME:
      Mix_ResumeMusic ( );
      break;
    case AUDIO_OPERATION::STOP:
      Mix_HaltMusic ( );
    default:
      break;
  }
}

inline void music_play ( Mix_Music *p, int loop = 1, const bool msFadeIn = 0 ) {
  int rtn = ( msFadeIn > 0 ) ? Mix_FadeInMusic ( p, loop, msFadeIn ) : Mix_PlayMusic ( p, loop );
  if ( -1 == rtn ) {
    throw SDL2Exception ( "Error playing music" );
  }
}

inline int music_volume ( int volume ) { return Mix_VolumeMusic ( volume ); }
inline int music_volume ( ) { return music_volume ( -1 ); }
inline void music_callback ( void ( *callback ) ( ) ) { Mix_HookMusicFinished ( callback ); }

inline bool is_music_playing ( ) { return music_state ( MUSIC_STATE::PLAYING ); }
inline bool is_music_paused ( ) { return music_state ( MUSIC_STATE::PLAYING ); }

inline void music_halt ( int msFade = 0 ) {
  if ( is_music_playing ( ) ) {
    ( msFade > 0 ) ? Mix_FadeOutMusic ( msFade ) : Mix_HaltMusic ( );
  }
}

inline void music_set_position ( double position = 0.0 ) {
  Mix_RewindMusic ( );
  if ( Mix_SetMusicPosition ( position ) == -1 ) {
    throw SDL2Exception ( "Error setting music position" );
  }
}

#endif
/**
 * SDL2 RENDER METHODS
 */
inline void clear ( SDL_Renderer *renderer, const SDL_Color &color = {0, 0, 0, 0} ) {
  SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
  SDL_RenderClear ( renderer );
}
/**
 *
 **/
inline auto get_viewport ( SDL_Renderer *renderer ) {
  SDL_Rect rec;
  SDL_RenderGetViewport ( renderer, &rec );
  return std::make_tuple ( rec.x, rec.y, rec.w, rec.h );
}
inline void set_viewport ( SDL_Renderer *renderer, int x, int y, int w, int h ) {
  SDL_Rect viewPort = {x, y, w, h};
  if ( SDL_RenderSetViewport ( renderer, &viewPort ) != 0 ) {
    throw SDL2Exception ( "Failed to set viewport." );
  }
}
/**
 *
 **/
inline auto get_scale ( SDL_Renderer *renderer ) {
  float scaleX, scaleY;
  SDL_RenderGetScale ( renderer, &scaleX, &scaleY );
  return std::make_tuple ( scaleX, scaleY );
}

inline void set_scale ( SDL_Renderer *renderer, float x, float y ) {
  if ( SDL_RenderSetScale ( renderer, x, y ) != 0 ) {
    throw SDL2Exception ( "Failed to set scale." );
  }
}
/**
 *
 **/
inline auto get_logical_size ( SDL_Renderer *renderer ) {
  int width, height;
  SDL_RenderGetLogicalSize ( renderer, &width, &height );
  return std::make_tuple ( width, height );
}
inline void set_logical_size ( SDL_Renderer *renderer, int width, int height, const bool enableIntegerScale = true ) {
  if ( SDL_RenderSetLogicalSize ( renderer, width, height ) != 0 ) {
    throw SDL2Exception ( "Failed to set logical size." );
  }
  if ( SDL_RenderSetIntegerScale ( renderer, enableIntegerScale ? SDL_TRUE : SDL_FALSE ) != 0 ) {
    throw SDL2Exception ( "Failed to set Integer scale." );
  }
}

inline void render_texture ( SDL_Renderer *renderer, SDL_Texture *texture,  //
                             const SDL_Rect *src = nullptr,                 //
                             const SDL_Rect *dst = nullptr ) {
  int rtn = SDL_RenderCopy ( renderer, texture, src, dst );
  if ( rtn != 0 ) {
    throw SDL2Exception ( "Failed to render texture." );
  }
}

inline void render_texture ( SDL_Renderer *renderer, SDL_Texture *texture,  //
                             const double angle,                            //
                             sdl2::FLIP flip = sdl2::FLIP::NONE,            //
                             const SDL_Point *angleCenter = nullptr,        //
                             const SDL_Rect *src = nullptr,                 //
                             const SDL_Rect *dst = nullptr ) {
  int rtn = SDL_RenderCopyEx ( renderer,     //
                               texture,      //
                               src,          //
                               dst,          //
                               angle,        //
                               angleCenter,  //
                               internal::to_type< SDL_RendererFlip > ( flip ) );
  if ( rtn != 0 ) {
    throw SDL2Exception ( "Failed to render texture." );
  }
}

template < typename FUNC >
inline bool render_to_pixel ( FUNC &&func, SDL_Texture *texture, SDL_Rect *rect = nullptr ) {
  bool rtn{false};
  std::uint32_t *pixels{nullptr};
  int pitch{0};

  if ( ( rtn = ( SDL_LockTexture ( texture, rect, ( void ** ) &pixels, &pitch ) == 0 ) ) ) {
    func ( pixels );
    SDL_UnlockTexture ( texture );
  }
  return rtn;
}

inline void set_renderer ( SDL_Renderer *renderer, SDL_Texture *texture ) {
  if ( SDL_FALSE == SDL_RenderTargetSupported ( renderer ) ) {
    throw SDL2Exception ( "Window does not support the use of render targets" );
  }
  if ( texture != nullptr ) {
    int access;
    SDL_QueryTexture ( texture, nullptr, &access, nullptr, nullptr );
    if ( internal::to_type ( TEXTUREACCESS::TARGET ) != access ) {
      throw SDL2Exception ( "Create the texture with TEXTUREACCESS::TARGET" );
    }
  }
  SDL_SetRenderTarget ( renderer, texture );
}
template < typename FUNC >
inline void render_to_texture ( FUNC &&func, SDL_Renderer *renderer, SDL_Texture *texture ) {
  set_renderer ( renderer, texture );
  clear ( renderer );
  func ( );
  set_renderer ( renderer, nullptr );
}
template < typename FUNC >
inline void render_to_texture_and_draw ( FUNC &&func, SDL_Renderer *renderer, SDL_Texture *texture ) {
  render_to_texture ( std::forward< FUNC > ( func ), renderer, texture );
  render_texture ( renderer, texture );
}

template < typename FUNC >
inline void render_call ( SDL_Renderer *renderer, FUNC &&func, const SDL_Color &color ) {
  SDL_BlendMode prevMode;
  SDL_Color prevColor;

  SDL_GetRenderDrawColor ( renderer, &prevColor.r, &prevColor.g, &prevColor.b, &prevColor.a );
  SDL_GetRenderDrawBlendMode ( renderer, &prevMode );

  SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );

  auto rtn = func ( );

  SDL_SetRenderDrawColor ( renderer, prevColor.r, prevColor.g, prevColor.b, prevColor.a );
  SDL_SetRenderDrawBlendMode ( renderer, prevMode );

  if ( rtn != 0 ) {
    throw SDL2Exception ( "Failed to render" );
  }
}

inline void render_line ( SDL_Renderer *renderer, const SDL_Point &point1, const SDL_Point &point2,
                          const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderDrawLine ( renderer, point1.x, point1.y, point2.x, point2.y ); },
                color );
}
inline void render_lines ( SDL_Renderer *renderer, const SDL_Point *points, int count, const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderDrawLines ( renderer, points, count ); }, color );
}
inline void render_point ( SDL_Renderer *renderer, const SDL_Point &point, const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderDrawPoint ( renderer, point.x, point.y ); }, color );
}
inline void render_points ( SDL_Renderer *renderer, const SDL_Point *points, int count, const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderDrawPoints ( renderer, points, count ); }, color );
}
inline void render_rect ( SDL_Renderer *renderer, const SDL_Rect &rec, const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderDrawRect ( renderer, &rec ); }, color );
}
inline void render_rects ( SDL_Renderer *renderer, const SDL_Rect *rects, std::size_t size, const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderDrawRects ( renderer, rects, size ); }, color );
}
inline void render_rect_fill ( SDL_Renderer *renderer, const SDL_Rect &rec, const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderFillRect ( renderer, &rec ); }, color );
}
inline void render_rects_fill ( SDL_Renderer *renderer, const SDL_Rect *rects, std::size_t size,
                                const SDL_Color &color ) {
  render_call ( renderer, [&]( ) { return SDL_RenderFillRects ( renderer, rects, size ); }, color );
}

}  // namespace sdl2
