#include <chrono>
#include <cstdlib>
#include <iostream>
#include <random>
#include "gd_sdl2_helper.hh"

constexpr auto FIRE_WIDTH{320};
constexpr auto FIRE_HEIGHT{168};

constexpr auto WIDTH{FIRE_WIDTH << 1};
constexpr auto HEIGHT{FIRE_HEIGHT << 1};

constexpr std::uint8_t rgbs[] = {0x07, 0x07, 0x07,  //
                                 0x1F, 0x07, 0x07,  //
                                 0x2F, 0x0F, 0x07,  //
                                 0x47, 0x0F, 0x07,  //
                                 0x57, 0x17, 0x07,  //
                                 0x67, 0x1F, 0x07,  //
                                 0x77, 0x1F, 0x07,  //
                                 0x8F, 0x27, 0x07,  //
                                 0x9F, 0x2F, 0x07,  //
                                 0xAF, 0x3F, 0x07,  //
                                 0xBF, 0x47, 0x07,  //
                                 0xC7, 0x47, 0x07,  //
                                 0xDF, 0x4F, 0x07,  //
                                 0xDF, 0x57, 0x07,  //
                                 0xDF, 0x57, 0x07,  //
                                 0xD7, 0x5F, 0x07,  //
                                 0xD7, 0x5F, 0x07,  //
                                 0xD7, 0x67, 0x0F,  //
                                 0xCF, 0x6F, 0x0F,  //
                                 0xCF, 0x77, 0x0F,  //
                                 0xCF, 0x7F, 0x0F,  //
                                 0xCF, 0x87, 0x17,  //
                                 0xC7, 0x87, 0x17,  //
                                 0xC7, 0x8F, 0x17,  //
                                 0xC7, 0x97, 0x1F,  //
                                 0xBF, 0x9F, 0x1F,  //
                                 0xBF, 0x9F, 0x1F,  //
                                 0xBF, 0xA7, 0x27,  //
                                 0xBF, 0xA7, 0x27,  //
                                 0xBF, 0xAF, 0x2F,  //
                                 0xB7, 0xAF, 0x2F,  //
                                 0xB7, 0xB7, 0x2F,  //
                                 0xB7, 0xB7, 0x37,  //
                                 0xCF, 0xCF, 0x6F,  //
                                 0xDF, 0xDF, 0x9F,  //
                                 0xEF, 0xEF, 0xC7,  //
                                 0xFF, 0xFF, 0xFF};

constexpr auto rgbSize{sizeof ( rgbs ) / sizeof ( std::uint8_t )};
constexpr auto colorPaletteSize{rgbSize / 3};

SDL_Color colorPalette[ colorPaletteSize ];
std::array< std::uint32_t, FIRE_WIDTH * FIRE_HEIGHT > firePixels;

struct Sdl2 {
  Sdl2 ( ) { sdl2::init_sdl ( true ); }
  virtual ~Sdl2 ( ) = default;
  sdl2::WindowUniqPtr window{nullptr};
  sdl2::RendererUniqPtr renderer{nullptr};
  sdl2::TextureUniqPtr fireTexture{nullptr};
  sdl2::PixelFormatUniqPtr pixelFormat{nullptr};
};

void init ( Sdl2& sdl2 ) {
  sdl2.window = sdl2::create_window< sdl2::WindowUniqPtr > ( {WIDTH, HEIGHT} );
  sdl2.renderer = sdl2::create_renderer< sdl2::RendererUniqPtr > (  //
      sdl2.window.get ( ),                                          //
      sdl2::BLENDMODE::NONE,                                        //
      sdl2::RENDERER::ACCELERATED | sdl2::RENDERER::VSYNC );
  sdl2.fireTexture = sdl2::create_texture< sdl2::TextureUniqPtr > (  //
      sdl2.window.get ( ),                                           //
      sdl2.renderer.get ( ),                                         //
      sdl2::TEXTUREACCESS::STREAMING,                                //
      FIRE_WIDTH, FIRE_HEIGHT );
  sdl2.pixelFormat = sdl2::create_pixelformat ( sdl2.window.get ( ) );

  // Init the color palette
  for ( auto i{0}; i < colorPaletteSize; ++i ) {
    colorPalette[ i ].r = rgbs[ i * 3 + 0 ];
    colorPalette[ i ].g = rgbs[ i * 3 + 1 ];
    colorPalette[ i ].b = rgbs[ i * 3 + 2 ];
  }

  firePixels.fill ( 0 );
  for ( auto i{0}; i < FIRE_WIDTH; ++i ) {
    firePixels[ ( FIRE_HEIGHT - 1 ) * FIRE_WIDTH + i ] = 36;
  }
  sdl2::set_window_title(sdl2.window.get(), "DoomFirePSX");
}

//
// Main
//
int main ( int argc, char* argv[] ) {
  auto random_number = []( int min, int max ) {
    const static auto seed = std::chrono::system_clock::now ( ).time_since_epoch ( ).count ( );
    static std::mt19937 gen ( seed );
    std::uniform_int_distribution<> dis ( min, max );
    return dis ( gen );
  };

  try {
    Sdl2 sdl2;
    init ( sdl2 );

    bool running{true};
    while ( running ) {
      SDL_Event event;
      while ( SDL_PollEvent ( &event ) != 0 ) {
        switch ( event.type ) {
          case SDL_QUIT:
            running = false;
            break;
          case SDL_KEYDOWN: {
            switch ( event.key.keysym.sym ) {
              case SDLK_ESCAPE:
                running = false;
                break;
              default:
                break;
            }
          } break;
          defaut:
            break;
        }
      }  // end of while
      //The Meat
      for ( auto i{0}; i < FIRE_WIDTH; ++i ) {
        for ( auto j{1}; j < FIRE_HEIGHT; ++j ) {
          auto srcIndex = j * FIRE_WIDTH + i;

          auto pixel = firePixels[ srcIndex ];
          if ( pixel == 0 ) {
            firePixels[ srcIndex - FIRE_WIDTH ] = 0;
          } else {
            auto randIndex = random_number ( 0, 3 );
            auto dstIndex = srcIndex - randIndex + 1;
            firePixels[ dstIndex - FIRE_WIDTH ] = pixel - ( randIndex & 1 );
          }
        }
      }
      sdl2::clear ( sdl2.renderer.get ( ) );
      sdl2::render_to_pixel (
          [&sdl2]( std::uint32_t* pixels ) {
            //
            for ( auto j{0}; j < FIRE_HEIGHT; ++j ) {
              for ( auto i{0}; i < FIRE_WIDTH; ++i ) {
                auto index = j * FIRE_WIDTH + i;
                auto colorIndex = firePixels[ index ];
                if ( colorIndex < colorPaletteSize ) {
                  pixels[ index ] = SDL_MapRGB ( sdl2.pixelFormat.get ( ),
                                                 colorPalette[ colorIndex ].r,  //
                                                 colorPalette[ colorIndex ].g,  //
                                                 colorPalette[ colorIndex ].b );
                }
              }
            }
          },
          sdl2.fireTexture.get ( ) );

      static const SDL_Rect rect{( WIDTH - FIRE_WIDTH ) / 2, ( HEIGHT - FIRE_HEIGHT ) / 2, FIRE_WIDTH, FIRE_HEIGHT};
      sdl2::render_texture ( sdl2.renderer.get ( ),     //
                             sdl2.fireTexture.get ( ),  //
                             nullptr,                   //
                             &rect );

      sdl2::swap ( sdl2.renderer.get ( ) );

    }  // end of while
  } catch ( const std::exception& e ) {
    std::cerr << e.what ( ) << std::endl;
  }
  return EXIT_SUCCESS;
}

